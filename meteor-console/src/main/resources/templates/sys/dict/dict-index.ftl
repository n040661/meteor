<#--
/****************************************************
 * Description: 数据字典的列表页面
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>

<@content id=tabId>
	<@query queryUrl="${base}/sys/dict/list" id=tabId>
		<@querygroup title='属性组'>
			<@select name="query.groupCode@lk@s" value="${dict.groupCode}" selectPage=true list=groupList listKey='groupName' listValue='groupCodeM'></@select>
		</@querygroup>
		<@querygroup title='名称'>
			<input type="search" name="query.name@lk@s" class="form-control input-sm" placeholder="请输入名称" aria-controls="dynamic-table">
	    </@querygroup>

		<@querygroup title='状态'>
			<@select name="query.status@eq@s" list=XJJConstants.COMMON_STATUS_LIST></@select>
	    </@querygroup>

		<@button type="info" icon="glyphicon glyphicon-search" onclick="XJJ.query({id:'${tabId}'});">查询</@button>
	</@query>


	<@button type="info" icon="glyphicon glyphicon-plus" onclick="XJJ.add('${base}/sys/dict/input','添加数据字典','${tabId}');">增加</@button>
	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/sys/dict/input','修改数据字典','${tabId}');">修改</@button>
	<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/sys/dict/delete','删除数据字典？',true,{id:'${tabId}'});">删除</@button>
</@content>
