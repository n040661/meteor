/****************************************************
 * Description: Service for t_meteor_machineinfo_privilege
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-03 reywong Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.meteor.machineinfo.service;

import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoPrivilegeEntity;

import java.util.List;
import java.util.Map;


public interface MachineinfoPrivilegeService extends XjjService<MachineinfoPrivilegeEntity> {

    /**
     * 导入
     *
     * @param fileId
     */
    Map<String, Object> saveImport(Long fileId) throws ValidationException;

    /**
     * 批量插入
     *
     * @param machineinfoPrivilegeEntityList
     * @return
     */
    Boolean batchInsert(List<MachineinfoPrivilegeEntity> machineinfoPrivilegeEntityList);

}
