/****************************************************
 * Description: DAO for 用户角色
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author RY
 * @version 1.0
 * @see
HISTORY
 *  2018-04-18 RY Create File
 **************************************************/
package cn.com.ry.framework.application.meteor.sec.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sec.entity.UserRoleEntity;
import org.apache.ibatis.annotations.Param;

public interface UserRoleDao extends XjjDAO<UserRoleEntity> {
    void deleteBy2Id(@Param("userId") Long userId, @Param("roleId") Long roleId);

    void deleteByUserId(@Param("userId") Long userId);

}

