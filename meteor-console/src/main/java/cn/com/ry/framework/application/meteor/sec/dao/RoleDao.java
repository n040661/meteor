/****************************************************
 * Description: DAO for 角色
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-18 RY Create File
**************************************************/
package cn.com.ry.framework.application.meteor.sec.dao;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sec.entity.RoleEntity;
import cn.com.ry.framework.application.meteor.sec.entity.RolePrivilegeEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleDao  extends XjjDAO<RoleEntity> {

	public List<RoleEntity> findListNoUser(@Param("userId") Long userId);

	public RoleEntity getByCode(@Param("code") String code);

	public List<RolePrivilegeEntity> findPrivilegeByRole(@Param("roleId") Long roleId);

	public List<RoleEntity> findListByUserId(@Param("userId") Long userId);
}

