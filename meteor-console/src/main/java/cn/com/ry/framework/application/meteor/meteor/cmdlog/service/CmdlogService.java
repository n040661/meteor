/****************************************************
 * Description: Service for t_meteor_cmdlog
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-05 reywong Create File
**************************************************/
package cn.com.ry.framework.application.meteor.meteor.cmdlog.service;
import cn.com.ry.framework.application.meteor.meteor.cmdlog.entity.CmdlogEntity;
import cn.com.ry.framework.application.meteor.framework.service.XjjService;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;

import java.util.Map;


public interface CmdlogService  extends XjjService<CmdlogEntity>{

    /**
    * 导入
    * @param fileId
    */
    Map<String,Object> saveImport(Long fileId) throws ValidationException;
}
