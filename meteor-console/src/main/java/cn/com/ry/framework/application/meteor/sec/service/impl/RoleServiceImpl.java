/****************************************************
 * Description: ServiceImpl for 角色
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      RY
 * @version     1.0
 * @see
	HISTORY
    *  2018-04-18 RY Create File
**************************************************/

package cn.com.ry.framework.application.meteor.sec.service.impl;

import cn.com.ry.framework.application.meteor.framework.dao.XjjDAO;
import cn.com.ry.framework.application.meteor.sec.dao.RoleDao;
import cn.com.ry.framework.application.meteor.sec.entity.RoleEntity;
import cn.com.ry.framework.application.meteor.sec.entity.RolePrivilegeEntity;
import cn.com.ry.framework.application.meteor.sec.service.RoleService;
import cn.com.ry.framework.application.meteor.framework.service.XjjServiceSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl extends XjjServiceSupport<RoleEntity> implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	public XjjDAO<RoleEntity> getDao() {

		return roleDao;
	}

	public List<RoleEntity> findListNoUser(Long userId)
	{
		return roleDao.findListNoUser(userId);
	}

	public RoleEntity getByCode(String code)
	{
		return roleDao.getByCode(code);
	}

	public List<RolePrivilegeEntity> findPrivilegeByRole(Long roleId)
	{
		return roleDao.findPrivilegeByRole(roleId);
	}


	public List<RoleEntity> findListByUserId(Long userId)
	{
		return roleDao.findListByUserId(userId);
	}
}
